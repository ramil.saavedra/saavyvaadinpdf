package org.saavy.ui;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.html.AnchorTarget;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.page.Page;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.InputStreamFactory;
import com.vaadin.flow.server.StreamResource;
import com.vaadin.flow.server.VaadinResponse;
import org.saavy.service.JasperReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.vaadin.olli.FileDownloadWrapper;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Base64;

/**
 * The main view contains a button and a click listener.
 */
@Route("")
public class MainView extends VerticalLayout {

    private final JasperReportService jasperReportService;

    /**
     * The Vaadin Main Page. The page has a text file that will reflect when you download the pdf file.
     * @param jasperReportService
     */
    @Autowired
    public MainView(JasperReportService jasperReportService) {
        this.jasperReportService = jasperReportService;
        // Use TextField for standard text input
        TextField textField = new TextField("Your name");
        // Button when click will generate the pdf file where the value of the text field will be displayed in the pdf file.
        Button generateReportButton = new Button("Generate Report");

        //Listener when the button Generate Report is clicked
        generateReportButton.addClickListener(event -> {
            //JSON string that will be fed to the PDF file via jasper report library.
            String jsonString = "{\"name\":\""+textField.getValue()+"\"}";
            //JSON object data parsed from the json string data.
            JsonObject jsonObject = JsonParser.parseString(jsonString).getAsJsonObject();

            //Calling jasperReportService to generate the pdf file.
            byte[] reportBytes = jasperReportService.generateReport(jsonObject);

            //Proceed only if reportBytes contains data.
            if (reportBytes != null) {
                //Encoding byte array to Base 64 String.
                String base64DataUri = "data:application/pdf;base64," + Base64.getEncoder().encodeToString(reportBytes);
                //JavaScript that will be executed when the pdf file is ready to be downloaded
                String clickScript = "var link = document.createElement('a');\n" +
                        "link.href = '" + base64DataUri + "';" +
                        "link.download = 'report.pdf';\n" + // Set the desired filename here
                        "link.style.display = 'none';\n" +
                        "document.body.appendChild(link);\n" +
                        "link.click();\n" +
                        "document.body.removeChild(link);";

                // Execute the JavaScript snippet
                UI.getCurrent().getPage().executeJs(clickScript);
            }
        });
        //Adding the textfield to the main vertical layout
        add(textField);
        //Adding the button to the main vertical layout
        add(generateReportButton);
    }
}
