package org.saavy.service;

import com.google.gson.JsonObject;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JsonDataSource;

import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

@Service
public class JasperReportService {
    public byte[] generateReport(JsonObject jsonData) {
        try {
            // Load the JasperReports template (replace "reportTemplatePath" with the actual path)
            InputStream template = getClass().getResourceAsStream("/reportTemplate.jrxml");

            // Compile the template
            JasperReport jasperReport = JasperCompileManager.compileReport(template);

            // Create a data source from the JSON object
            JsonDataSource dataSource = new JsonDataSource(new ByteArrayInputStream(jsonData.toString().getBytes()));

            // Set up report parameters if needed
            // Map<String, Object> params = new HashMap<>();
            // params.put("parameterName", value);

            // Generate the report
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, null, dataSource);

            // Export the report to a byte array (e.g., PDF)
            byte[] pdfReport = JasperExportManager.exportReportToPdf(jasperPrint);

            return pdfReport;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}


