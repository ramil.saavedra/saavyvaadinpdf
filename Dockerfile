FROM amazoncorretto:18-alpine
VOLUME /tmp
COPY build/libs/SaavyVaadinPDF.jar app.jar
RUN apk add --no-cache msttcorefonts-installer fontconfig
RUN update-ms-fonts
ENTRYPOINT ["java","-jar","/app.jar"]
EXPOSE 8080